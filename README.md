# Job Utils

This repo contains various files (e.g., JDLs, bash scripts, output files) which helped me to run jobs and to analyze their output in the ALICE grid.

## Setup

The setup is done using [these instructions](https://gitlab.cern.ch/mariaele/jalien-user-setup).
