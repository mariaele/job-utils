CPU_STRING=$1
SEARCH_PATH=$2
SITE=$3

for i in $(grep --color=auto "$CPU_STRING" -rn "$SEARCH_PATH" | awk -F':' '{print $1}' | sort -u);
do
	if [[ $# -ge 3 ]]
	then
		if [[ $(grep "$SITE" $i | wc -l) -eq 0 ]]
		then
			continue;
		fi
	fi
	echo $i;
	head -n 2 $i;
	grep --color=auto "Info: model:" $i;
	grep --color=auto -A 2 "Machine" $i;
	grep --color=auto -i "NUMA" $i;
done
