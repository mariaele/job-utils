# How to run

## Step 1: Clone/Download inxi repo from [here](https://github.com/smxi/inxi).

```
$ git clone https://github.com/smxi/inxi
```

## Step 2: Copy files to grid using `alien.py`.

The files that must be copied to grid:
  - `inxi` - from the inxi repo; this tool helps retrieving various informations
from the host.
  - `inxi.sh` - from this repo; this script gets the CE, the hostname and runs
the inxi tool.
  - `inxi_s.jdl` - from this repo; this jdl starts 20 jobs on the grid using
`inxi.sh` as a starting point.
  - `inxi.jdl` - from this repo; this jdl starts `$2` jobs that must run on the
CE indicated by `$3` and the output must be saved in the directory indicated by
`$1`.

```
[xjalienfs] ~> alien.py cp -f file:"filename" "filename"
```

## Step 3: Submit job

You can either use `inxi_s.jdl` or `inxi.jdl` with the correct arguments.

```
AliEn[<user>]:/alice/cern.ch/user/<u>/<user>/ >submit inxi.jdl output 20 ALICE::UPB::SLURM
```

## Step 4: Inspect the results from the output file
